"use strict";

let createNewUser = function () {
  let newUser = {
    _firstName: prompt("What is your first name?"),
    _lastName: prompt("What is your  last name?"),
    birthday: prompt("Enter your birthday", "dd.mm.yyyy"),

    getLogin: function () {
      let result =
        this._firstName[0].toLowerCase() + this._lastName.toLowerCase();
      return result;
    },

    getAge: function () {
      let now = new Date();
      let userBirthday = new Date(
        this.birthday.substr(6, 4),
        this.birthday.substr(3, 2) - 1,
        this.birthday.substr(0, 2)
      );
      let userBd = Math.trunc(
        (now.getTime() - userBirthday.getTime()) / 1000 / 60 / 60 / 24 / 365
      );
      alert(`You are ${userBd} years old!`);
      return userBd;
    },

    getPassword: function () {
      let getPassword =
        this._firstName[0].toUpperCase() +
        this._lastName.toLowerCase() +
        this.birthday.substr(6, 4);
      return getPassword;
    },
  };

  return newUser;
};

Object.defineProperty(createNewUser, "_firstName", {
  writable: false,
});
Object.defineProperty(createNewUser, "_lastName", {
  writable: false,
});

Object.defineProperty(createNewUser, "firstName", {
  get() {
    return this._firstName;
  },
  set(value) {
    this._firstName = value;
  },
});
Object.defineProperty(createNewUser, "lastName", {
  get() {
    return this._lastName;
  },
  set(value) {
    this._lastName = value;
  },
});

let firstUser = createNewUser();
console.log(firstUser.getLogin());
console.log(firstUser.getAge());
console.log(firstUser.getPassword());
